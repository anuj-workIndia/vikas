import React from 'react';
import { enableScreens } from 'react-native-screens';
import LanguageSelection from './src/views/startup/LanguageSelection';
import SplashScreen from './src/views/startup/SplashScreen';


enableScreens();

const App = () => {
  return (
    <>
      <SplashScreen></SplashScreen>
    </>
  );
};


export default App;
