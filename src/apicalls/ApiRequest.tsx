import axios from "axios"
import { UrlConfig } from "./UrlConfig";

let headers = {
    "Content-Type": "application/json",
};
const apiClient = axios.create({
    baseURL: UrlConfig.baseURL,
    headers: headers,
    timeout: 3000
})

function loginRegister(json: {}) {
    return apiClient.post('customer/app/register/login/', json)
}


async function callAPI(apiFuntion: any) {
    try {
        let response = { ...(await apiFuntion) }
        if (response.status === 200) {
            return response.data
        } else {
            return undefined
        }
    } catch (err) {
        return err
    }

}

export const ApiRequest = {
    callAPI,
    loginRegister
}