import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { useMemo, useState } from 'react';
import { StatusBar, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { AuthContext } from '../context/AuthContext';
import Colors from '../theme/Colors';
import Home from '../views/home/Home';
import Login from '../views/login/Login';
import LanguageSelection from '../views/startup/LanguageSelection';
import Ionicons from 'react-native-vector-icons/Ionicons'
import FeatherIcon from 'react-native-vector-icons/SimpleLineIcons'
import Fontisto from 'react-native-vector-icons/Fontisto'
import AppTextView from '../components/AppTextView';


// Stacks
const Stack = createStackNavigator();
const TabsNavigator = createBottomTabNavigator();
const HomeStack = createStackNavigator();



const Tabs = () => {

    return (
        <TabsNavigator.Navigator
            tabBarOptions={{
                activeTintColor: Colors.primary,
                inactiveTintColor: 'gray',
                showLabel: false,

            }}


            screenOptions={({ route }) => ({

                tabBarIcon: ({ focused, color, size }) => {
                    var iconName: string = "";

                    if (route.name === 'shops') {
                        iconName = focused
                            ? 'home'
                            : 'home-outline';
                        return (
                            <View style={{ alignItems: 'center' }}>
                                <Ionicons name={iconName} size={size} color={color} />
                                <AppTextView>Shops</AppTextView>
                            </View>
                        )


                    } else if (route.name === 'profile') {

                        return (
                            <View style={{ alignItems: 'center' }}>
                                {focused ? <Fontisto name="player-settings" size={size} color={color} /> : <FeatherIcon name="settings" size={size} color={color} />}
                                <AppTextView>Profile</AppTextView>
                            </View>
                        )


                    }


                },
            })}>
            <TabsNavigator.Screen name="shops" component={Home} options={{ title: 'Shops' }}></TabsNavigator.Screen>
            <TabsNavigator.Screen name="profile" component={Login} options={{ title: 'Profile' }}></TabsNavigator.Screen>
        </TabsNavigator.Navigator>
    )
}


const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="home" component={Home} options={{ title: "Shops" }}></HomeStack.Screen>
            <HomeStack.Screen name="homeDetails" component={Loi}></HomeStack.Screen>
        </HomeStack.Navigator>)
}

const AppNavigation = () => {
    const [auth, setAuth] = useState({})

    const authContext = useMemo(() => ({
        loginIn: () => {
            setAuth({
                token: 'abc'
            })
        },
        logOut: () => {
            setAuth({
                token: null
            })
        },
        auth
    }), [auth])




    return (
        <AuthContext.Provider value={authContext}>
            <SafeAreaProvider>
                <StatusBar backgroundColor={Colors.primary_dark} barStyle="light-content"></StatusBar>
                <NavigationContainer>
                    {(auth as any).token ?
                        <Stack.Navigator
                            screenOptions={{
                                headerStyle: {
                                    backgroundColor: Colors.primary,
                                },
                                headerTintColor: 'white'
                            }}>
                            <Stack.Screen name="login" component={login} options={{ title: 'Hello' }} />
                        </Stack.Navigator> :
                        <Stack.Navigator
                            screenOptions={{
                                headerStyle: {
                                    backgroundColor: Colors.primary,
                                },
                                headerTintColor: 'white'
                            }}>
                            <Stack.Screen name="homeTabs" component={Tabs} options={{ title: 'Hello', headerShown: false }} />
                        </Stack.Navigator>}

                </NavigationContainer>
            </SafeAreaProvider>
        </AuthContext.Provider>
    )
}

export default AppNavigation
