import React from 'react'
import { ActivityIndicator, Pressable, StyleSheet } from 'react-native'
import Colors from '../theme/Colors'
import AppTextView from './AppTextView'


interface Props {
    title: string
    styleButton?: {}
    styleText?: {}
    isDisabled?: boolean,
    isLoading?: boolean,
    onClicked?(): any
}
const AppButton = ({ title, styleButton, styleText, onClicked, isLoading, isDisabled, ...props }: Props) => {
    console.log("Button Loaded");
    return (
        <Pressable
            style={({ pressed }) => [{ opacity: pressed ? 0.7 : 1 }, styles.default, styleButton, {
                backgroundColor: isDisabled ? 'grey' : Colors.primary_dark
            }]}
            onPress={onClicked}
            disabled={isDisabled}
            android_ripple={{ color: 'white' }}
            {...props}

        >
            {isLoading ? <ActivityIndicator size="small" color="white"></ActivityIndicator> : <AppTextView style={[styles.defautText]}>{title}</AppTextView>}

        </Pressable>
    )
}

const styles = StyleSheet.create({
    default: {
        height: 50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 8,
        elevation: 5,
        backgroundColor: Colors.primary_dark
    },
    defautText: {
        color: "white",
        fontSize: 18,
    }
})

export default AppButton
