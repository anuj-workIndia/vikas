import { Observer } from 'mobx-react-lite'
import React from 'react'
import { StyleSheet, TextInput, View } from 'react-native'
import Colors from '../theme/Colors'
import AppTextView from './AppTextView'


interface Props {
    placeHolder: string,
    containerStyle?: any
    inputStyle?: any,
    errorText?: string
    // All other props
    [x: string]: any;
}
const AppTextInput = ({ placeHolder, containerStyle, inputStyle, errorText, ...props }: Props) => {
    return (
        <View style={[styles.containerStyle, containerStyle]}>
            <TextInput placeholder={placeHolder} style={[styles.default, inputStyle, { borderColor: errorText ? 'red' : Colors.divider }]} {...props}></TextInput>
            {errorText ? <AppTextView style={{ padding: 5, color: 'red', fontSize: 12 }}>{errorText}</AppTextView> : null}
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        height: 40,
        width: '100%'

    },
    default: {
        borderColor: Colors.divider,
        borderBottomWidth: 1,
        padding: 10,
        color: Colors.text_secondary,
        fontSize: 14
    },
    defautText: {
        color: "white",
        fontSize: 18,
    }
})

export default AppTextInput
