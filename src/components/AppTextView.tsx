import React, { ReactNode } from 'react'
import { StyleSheet, Text } from 'react-native'
import Colors from '../theme/Colors'

interface Props {
    children: ReactNode
    style?: any
}

const AppTextView = ({ children, style, ...props }: Props) => {
    return (
        <Text style={[styles.default, style]} {...props}>{children}</Text>
    )
}

const styles = StyleSheet.create({
    default: {
        fontSize: 14,
        color: Colors.text_secondary
    }
})

export default AppTextView
