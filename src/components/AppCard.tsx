import React, { ReactNode, ReactChildren, ReactChild } from 'react'
import { StyleSheet, View } from 'react-native'

interface AppCardProps {
    children: ReactNode
    otherStyles?: {}
}

const AppCard = ({ children, otherStyles }: AppCardProps) => {
    return (
        <View style={[styles.container, otherStyles]}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        borderRadius: 7,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        elevation: 5,
        flex: 1,
        

    }
})

export default AppCard
