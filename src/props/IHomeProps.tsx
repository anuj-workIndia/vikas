export default interface IHomeProps {
    data: {
        id: string,
        url: string,
        shopName: string,
        ownerName: string,
        category: string,
        isFavourited: boolean
    },
    pos?: number,
    total?: number,
    onFavClick: () => void
    onCardClick: () => void
}