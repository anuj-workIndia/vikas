import React from 'react'

interface Props {
    auth: {},
    loginIn: () => void
    logOut: () => void

}

export const AuthContext = React.createContext({} as Props) 