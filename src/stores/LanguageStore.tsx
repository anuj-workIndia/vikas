
export function languageStore() {
    return {
        languages: [
            {
                "id": "en",
                "value": "English",
                isSelected: false
            },
            {
                "id": "hi",
                "value": "हिन्दी",
                isSelected: false
            },
            {
                "id": "mr",
                "value": "Marathi",
                isSelected: false
            }
        ],
        updateItem(index) {
            this.languages[index].isSelected = !this.languages[index].isSelected
        }
    }

}