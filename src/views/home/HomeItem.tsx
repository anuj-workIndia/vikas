import React from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome'
import AppCard from '../../components/AppCard'
import AppTextView from '../../components/AppTextView'
import IHomeProps from '../../props/IHomeProps'
import Colors from '../../theme/Colors'
import { Observer } from 'mobx-react-lite'



const HomeItem = ({ data, onFavClick, onCardClick }: IHomeProps) =>

    <Observer>{() => {
        return (
            <TouchableOpacity
                activeOpacity={0.9}
                onPress={onCardClick}
                key={data.id} style={styles.containerStyle}>
                <AppCard otherStyles={styles.card}>
                    <View style={styles.imageOverlay}></View>
                    <FastImage
                        style={styles.shopImg}
                        source={{
                            uri: `${data.url}`,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                    <View style={styles.categoryContainer}>
                        <AppTextView style={styles.categoryText}>{data.category.toUpperCase()}</AppTextView>
                    </View>
                    <Icon
                        onPress={onFavClick}
                        name={data.isFavourited ? 'heart' : 'heart-o'}
                        size={25}
                        style={{ position: 'absolute', right: 0, margin: 10, padding: 10, zIndex: 20 }}
                        color={`${data.isFavourited ? 'red' : 'white'}`}
                    />

                    <View style={{ padding: 10 }}>
                        <AppTextView style={styles.shopName}>{data.shopName}</AppTextView>
                        <View style={styles.ownerContainer}>
                            <View style={styles.ownerAvatarContainer}>
                                <FastImage
                                    style={styles.ownerAvatar}
                                    source={{
                                        uri: `${data.url}`,
                                        priority: FastImage.priority.normal,
                                    }}
                                    resizeMode={FastImage.resizeMode.cover}
                                />
                            </View>
                            <AppTextView style={styles.ownerName}>{data.ownerName}</AppTextView>
                        </View>

                    </View>

                </AppCard>
            </TouchableOpacity>)
    }}</Observer>




export default HomeItem

const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 5,
        marginHorizontal: 15,
        height: 280,
        marginBottom: 5
    },
    card:
    {
        backgroundColor: 'white',
        marginBottom: 5,
    },
    categoryContainer: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: Colors.primary,
        borderRadius: 10,
        position: 'absolute',
        zIndex: 99,
        margin: 10
    },
    categoryText: {
        color: 'white',
        fontSize: 12
    },
    imageOverlay: {
        height: 180,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        zIndex: 1,
        borderTopRightRadius: 7,
        borderTopLeftRadius: 7
    },
    shopImg: {
        width: '100%',
        height: 180,
        borderTopRightRadius: 7,
        borderTopLeftRadius: 7
    },
    shopName: {
        fontSize: 20,
        fontWeight: '600',
        color: Colors.primary
    },
    ownerContainer: {

        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
    },
    ownerAvatarContainer: { width: 40, height: 40, borderRadius: 20, },
    ownerAvatar: { width: '100%', height: '100%', borderRadius: 20 },
    ownerName: { fontSize: 16, marginLeft: 5, fontWeight: '400' }


})
