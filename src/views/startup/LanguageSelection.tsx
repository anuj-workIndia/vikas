import { Observer, useLocalStore } from 'mobx-react-lite'
import React from 'react'
import { FlatList, Pressable, StyleSheet, View } from 'react-native'
import AppButton from '../../components/AppButton'
import AppTextView from '../../components/AppTextView'
import Colors from '../../theme/Colors'
import Icon from 'react-native-vector-icons/AntDesign'


interface Items {
    item: {
        id: string,
        value: string,
        isSelected: boolean
    },
    index: number

}


const LanguageSelection = () => {

    const store = useLocalStore(() => ({
        languages: [
            {
                "id": "en",
                "value": "English",
                isSelected: false
            },
            {
                "id": "hi",
                "value": "हिन्दी",
                isSelected: false
            },
            {
                "id": "mr",
                "value": "Marathi",
                isSelected: false
            }
        ],
        updateItem(index: number) {
            store.languages.map(item => {
                item.isSelected = false
            })
            store.languages[index].isSelected = !store.languages[index].isSelected
            store.isButtonDisable = !(store.languages.filter(item => item.isSelected).length === 1)

        },
        isButtonDisable: true
    }))



    const renderItem = ({ item, index }: Items) =>
        <Observer>{() => {
            return (
                <Pressable
                    style={({ pressed }) => [styles.listItem, { opacity: pressed ? 0.5 : 1 }]}
                    key={item.id}
                    onPress={() => {
                        store.updateItem(index)
                    }}>
                    <AppTextView style={[styles.listItemText,
                    {
                        color: item.isSelected ? Colors.primary_dark : Colors.text_secondary,
                        fontWeight: item.isSelected ? '700' : '400'
                    }]}>{item.value}</AppTextView>
                    {item.isSelected ? <Icon name="check" size={20} color={Colors.primary_dark}></Icon> : null}

                </Pressable>
            )
        }
        }</Observer>




    const keyExtractor = (item: { id: any }) => item.id

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexGrow: 1 }}>
                <FlatList
                    data={store.languages.slice()}
                    renderItem={renderItem}
                    keyExtractor={keyExtractor}
                >
                </FlatList>
            </View>
            <Observer>
                {() =>
                    <AppButton
                        styleButton={styles.button}
                        title="Okay"
                        isDisabled={store.isButtonDisable}
                    ></AppButton>}
            </Observer>

        </View>
    )
}

const styles = StyleSheet.create({
    listItem: {
        padding: 20,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: Colors.divider
    },
    button: {
        position: 'absolute',
        bottom: 0,
        width: "100%",
    },
    listItemText: {
        fontSize: 18,
        color: Colors.text_primary
    }

})



export default LanguageSelection
