import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import Colors from '../../theme/Colors'
import { SafeAreaView } from 'react-native-safe-area-context'
import AppNavigation from '../../navigation/AppNavigation'
import AppTextView from '../../components/AppTextView'

const SplashScreen = () => {
    const [isAuthorised, setIsAuthorised] = useState<boolean>(false)

    useEffect(() => {
        setTimeout(() => {
            setIsAuthorised(true)
        }, 1000)
    }, [])

    if (!isAuthorised) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <AppTextView>Hello</AppTextView>
            </View>
        )
    }
    return (
        <AppNavigation></AppNavigation>
    )
}

export default SplashScreen
