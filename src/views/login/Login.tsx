import { Observer, useLocalStore } from 'mobx-react-lite'
import React from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import AppButton from '../../components/AppButton'
import AppTextInput from '../../components/AppTextInput'
import AppTextView from '../../components/AppTextView'
import Colors from '../../theme/Colors'
import { ApiRequest } from '../../apicalls/ApiRequest'

const Login = ({ navigation }: any) => {

    const store = useLocalStore(() => ({

        mobileNumber: '',
        password: '',
        mobileNumberError: '',
        passwordError: '',
        yesClicked: false,
        noClicked: false,
        isLoading: false,


        updateMobileNumber(text: string) {
            store.mobileNumberError = ""
            store.mobileNumber = text
        },
        updatePassword(text: string) {
            store.passwordError = ""
            store.password = text
        },
        checkMobileError() {
            if (store.mobileNumber.length > 0 && store.mobileNumber.length < 10) {
                this.mobileNumberError = "Please enter valid number"
            }
        },
        checkPasswordError() {

        },

        onClickAddShop(isYes: boolean) {
            if (isYes) {
                store.noClicked = false
                store.yesClicked = !store.yesClicked
            } else {
                store.yesClicked = false
                store.noClicked = !store.noClicked
            }
        },
        callLoginRegisterApi() {
            store.isLoading = true
            let json = {
                mobile_no: store.mobileNumber,
                password: store.password
            }

            ApiRequest.callAPI(ApiRequest.loginRegister(json)).then(response => {


            }).catch(err => {

            }).finally(() => {
                store.isLoading = false
            })

        },

        get isButtonDisable() {
            return !store.mobileNumber ||
                !store.password ||
                store.mobileNumber.length < 10 ||
                store.password.length < 4

        }
    }))






    return (
        <Observer>{() =>
            <View
                pointerEvents={store.isLoading ? "none" : "auto"}
                style={styles.container}>
                <AppTextView style={styles.loginRegisterText}>Please Login/Register to continue</AppTextView>


                <AppTextInput
                    placeHolder="Mobile Number"
                    containerStyle={styles.input}
                    keyboardType={"phone-pad"}
                    maxLength={10}
                    errorText={store.mobileNumberError}
                    value={store.mobileNumber}
                    onChangeText={store.updateMobileNumber}
                    onEndEditing={store.checkMobileError}


                />
                <AppTextInput
                    placeHolder="Password"
                    containerStyle={styles.input}
                    secureTextEntry={true}
                    maxLength={6}
                    errorText={store.passwordError}
                    value={store.password}
                    onChangeText={store.updatePassword}
                    onEndEditing={store.checkPasswordError}
                />


                <AppTextView style={styles.addShopText}>Do you own a shop? Want to add your shop?</AppTextView>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={() => {
                            store.onClickAddShop(false)
                        }}
                        style={[styles.buttonView, {
                            backgroundColor: store.noClicked ? Colors.primary_dark : 'transparent',

                        }]}>
                        <AppTextView style={{ color: store.noClicked ? 'white' : Colors.text_secondary }}>NO</AppTextView>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={() => {
                            store.onClickAddShop(true)
                        }}
                        style={[styles.buttonView, {
                            backgroundColor: store.yesClicked ? Colors.primary_dark : 'transparent',
                        }]}>
                        <AppTextView style={{ color: store.yesClicked ? 'white' : Colors.text_secondary }}>YES</AppTextView>
                    </TouchableOpacity>
                </View>
                <AppButton title="Login / Register"
                    isDisabled={store.isButtonDisable}
                    isLoading={store.isLoading}
                    onClicked={store.callLoginRegisterApi}></AppButton>
            </View >
        }</Observer>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20
    },
    loginRegisterText: {
        fontSize: 20,
        textAlign: 'center',
        fontWeight: '600',
    },
    input: {
        marginTop: 20
    },
    addShopText: {
        marginTop: 20,
        fontWeight: '600'
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 5
    },
    buttonView: {
        flex: 1,
        padding: 10,
        marginBottom: 20,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Colors.divider
    }




})

export default Login
